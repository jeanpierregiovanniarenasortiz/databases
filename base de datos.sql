crear la base de datos

CREATE DATABASE prueba1;
USE prueba1;

crear la tabla

CREATE TABLE pruebaA( a INT(10),
b CHAR(20),
c VARCHAR(20),
d ENUM('M','F')
PRIMARY KEY(a)
) ENGINE= innodb;

para agregar la llave primaria en la tabla 

ALTER TABLE pruebaA;
pruebaA ADD PRIMRARY KEY (a);

eliminar la llave primaria que  tiene para asignar la que es

ALTER TABLE pruebaA DROP PRIMARY KEY;
 
para agregar una llave primaria compuesta en la tabla

ALTER TABLE pruebaA ADD PRIMARY KEY(a,b);

para cambiar el nombre de una fila

ALTER TABLE pruebaA CHANGE c xyz VARCHAR(25);

para cambiar el nombre de la tabla 

RENAME TABLE nombre_tabla TO dato;

otra forma es: ALTER TABLE pruebaA RENAME dato;

para eliminar la tabla es:
 DROP TABLE pruebaA;

para eliminar la base de datos

DROP DATABASE prueba1;