para hacer consultas:


seleccionar los datos de persona

select *from persona;

para consultar  la id, nombre1, nombre2, apellido1,apellido2 y de manera decendente desde apellido


select idpersona, nombre1, nombre2,apellido1, apellido2 from persona
order by 4 desc;

select idpersona, nombre1, nombre2,apellido1, apellido2 from persona
order by apellido1 desc;

traer las personas nacidas antes del 8/9/89

select idpersona, nombre1, nombre2,apellido1, apellido2 from persona
where fecha_naci <= "1989-9-8";

traer el nombre concatenado que una sola columna salga los dos nombres y que el genero sea femenino

select CONCAT(nombre1,' ',nombre2)as nombre from persona
where fidgenero=1;

traer el a�o de nacimiento de cada persona,con los demas datos, y cambiarle el nombre a la fecha de nacimiento en year

select idpersona, nombre1, nombre2, apellido1,apellido2, year(fecha_naci) as year from persona;

traer nombre1 y nombre2, apellido1 y apellido2 en una sola columna cada una la fecha de nacimiento

select concat(nombre1,' ',nombre2) as nombre, concat(apellido1,' ',apellido2) as apellido, fecha_naci as fecha from persona;

cuando hay nombres nullos se hace de la siguiente forma

 select concat_ws(' ',apellido1,apellido2) as apellido from persona;

traer el nombre y el genero con unos join

join el salto de las llaves primarias  en las tablas

tabla a y tabla b

ta inner join tb on ta.A1=tb.A1

para optimizar es mejor:

 el inner es la comparacion y el join es el salto de una tabla a la otra tabla

on tb.A= ta.A;

select concat(nombre1,' ',nombre2) as nombre,direccion, fecha_naci, gene.nombre_gene from persona as perso inner join genero as  gene
on perso.fidgenero=gene.idgenero ;

buscar los datos de la persona, con sus respectivos generos, el nombre de usuario, fecha de ingreso y el area de trabajo

select concat(nombre1,' ',nombre2) as nombre,concat(apellido1,' ',apellido2) as apellido,direccion, fecha_naci, 
gene.nombre_gene,emple.nombre_b_usuario, emple.fecha_ingreso, area.nombre_area_traba from persona as perso inner join genero as  gene
on perso.fidgenero=gene.idgenero  inner join empleado as emple
on emple.fidpersona=perso.idpersona inner join area_trabajo as area
on emple.fidareatrabajo=area.idareatrabajo;


tarea 

1) hacer la consulta de los productos comprados, mostrando el empleado que lo vendio y su respectiva area de trabajo,
 con su respectivo genero y el proveedor que suministro ese producto.

select ele.nombre_elemento as producto,perso1.nombre1 as cliente, 
empl.nombre_b_usuario as empleado,gen.nombre_gene as genero,
area.nombre_area_traba as 'area de trabajo',perso2.nombre1 as proveedor from producto as produ inner join elemento as ele
on produ.fidelemento= ele.idelemento inner join proveedor as pro
on produ.fidproveedor= pro.idproveedor inner join detalle_venta as deven
on deven.fidproducto=produ.idproducto inner join factura_venta as factven
on deven.fidfacturaventa= factven.idfacturaventa inner join cliente as cli
on factven.fidcliente= cli.idcliente inner join persona as perso1
on cli.fidpersona= perso1.idpersona inner join empleado as empl
on factven.fidempleado= empl.idempleado inner join persona as perso
on empl.fidpersona=perso.idpersona inner join genero as gen
on perso.fidgenero= gen.idgenero inner join area_trabajo as area
on empl.fidareatrabajo= area.idareatrabajo inner  join persona as perso2
on pro.fidpersona= perso2.idpersona;


2)descripcion de producto

select  deta.cantidad_suministrada as cantidad, ele.nombre_elemento as producto,  
uni.unidad as unidad, prove.nombre_empresa as proveedor, sub.nombre_subcategoria as subcategoria,
cate.nombre_categoria as categoria from producto as produ inner join detalle_suministro as deta
on deta.fidproducto= produ.idproducto inner join elemento as ele
on produ.fidelemento= ele.idelemento inner join unidad_presentacion as uni
on produ.fidunidadpresentacion= uni.idunidadpresentacion inner join proveedor as prove
on produ.fidproveedor=prove.idproveedor inner join subcategoria as sub
on ele.fidsubcategoria=sub.idsubcategoria inner join categoria as cate
on sub.fidcategoria=cate.idcategoria;


3)fecha de vencimiento, debe salir proximo a vencer de lo contrario Ok, 30 dias

select nombre_elemento as producto,produ. canti_present as 'cantidad presente', deta.fecha_vensumin as 'fecha de vencimiento',
if(deta.fecha_vensumin<=curdate(),'Ok','Proximo a vencer') as 'productos vencidos' from elemento as ele inner join producto as produ
on produ.fidelemento=ele.idelemento inner join detalle_suministro as deta
on deta.fidproducto= produ.idproducto;


select fecha_vensumin as 'fecha de vencimiento',
if(fecha_vensumin<=curdate(),'Ok','Proximo a vencer') as 'Prodcutos vencidos' from detalle_suministro;



consultas  en clase 03/03/2015



 traer el nombre de la persona, el nombre de usuario del empleado, traer todas las personas y si es empleado debe salir el nombre de usaurio

select concat(nombre1,' ',nombre2) as nombre,if(em.nombre_b_usuario IS NULL,' ', em.nombre_b_usuario) as usuario, if(em.fidpersona= perso.idpersona,'empleado','no es empleado') as tipo
from persona as perso left join empleado em
on em.fidpersona= perso.idpersona;

 cuando se utiliza el left y el right

 para que me traiga todos los de la tabla izquierda left

para que me traiga todos los de la tabla derecha right



traer las personas que tiene facturas pendientes

select concat(nombre1,' ',nombre2) as nombre, cre.estado_credito as 'estado credito'
from persona as perso inner join cliente cli
on perso.idpersona= cli.fidpersona inner join factura_venta as fact
on cli.idcliente= fact.fidcliente left join credito as cre
on fact.idfacturaventa= cre.fidfacturaventa;


tarea

2) total de facturas > q 1.000.000

select count(valor_total_venta) as contador from factura_venta
where(valor_total_venta>1000000);



3) identificar cual es categoria y cual no es subcategoria


select  nombre_categoria  as 'nombre de productos', 'categoria' from categoria union 
select  nombre_subcategoria, 'subcategoria' from subcategoria;




consultas de agregacion:

cuales son 
max= maximo
min= minimo
avg o averege= promedio
count= cantidad


por ejemplo traer las cantidad por la fechas


select fecha count(*)...

group by fecha;

cuando vamos a traer la fecha mas reciente y la fecha mas antigua 

select max(fecha), min(fecha)


necesito conocer  cuantos empleados hay por area de trabajo 


select  count(ar.idareatrabajo) as contador,ar.nombre_area_traba as 'nombre de area' from empleado as em inner join area_trabajo as ar
on ar.idareatrabajo= em.fidareatrabajo
where ar.idareatrabajo=1;


traer todas las areas de trabajo  y contar los empleados 

select  count(idempleado) as contador,ar.nombre_area_traba as 'nombre de area' 
from empleado as em right join area_trabajo as ar
on ar.idareatrabajo= em.fidareatrabajo
group by ar.nombre_area_traba;



traer las fechas de la factura mas reciente  y factura mas antigua

select max(fecha_venta) as 'reciente', min(fecha_venta) as 'mas antigua' from factura_venta;


de la fecha mas antigua  le suma 5 y apartir de ahi  sacar el promedio de ellas


select avg(valor_total_venta) as 'Promedio' from factura_venta
where fecha_venta >=((select min(fecha_venta) from factura_venta) + interval 5 day);



tarea

1) necesito los datos de la factura las mas antigua y las reciente en cuento a fecha



2) coger un intervalo de fechas, sacar lo que ha ingresado


