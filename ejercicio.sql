CREATE DATABASE ejercicio;

USE ejercicio;

CREATE TABLE actor( nombre VARCHAR(10),
codigo INT (10),
nacionalidad VARCHAR(30),
PRIMARY KEY (codigo)
) ENGINE = innodb;

CREATE TABLE personaje( nombre VARCHAR(10),
codigo INT (10),
grado  INT(10),
raza  VARCHAR(20),
codigo_actor INT(10),
codigo_superior INT(10),
PRIMARY KEY (codigo)
) ENGINE = innodb;

CREATE TABLE personajepelicula(codigo_personaje INT(10),
codigo_pelicula INT(10)

