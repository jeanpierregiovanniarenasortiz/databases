para el lenguaje DML

INSERT INTO genero values
(1,'mujer'),
(2,'hombre'),
(3,'indefinido');


INSERT INTO persona values
(1,'jean pierre','giovanni','arenas','ortiz','carrera 11A','1992-02-18',2),
(2,'julian','andres','giraldo','ortiz','calle 11A','1990-05-28',2),
(3,'eliana','','cordoba','','calle 12A','1996-01-20',1),
(4,'judith','','ortiz','','calle 15B','1986-01-20',1),
(5,'juan','Nicolas','Abarca','Bayona','calle 12C','1990-10-3',2),
(6,'Rocio del','Pilar','Agonh','Pedraza','carrera 5B','1989-06-1',1),
(7,'Beatriz','Elena','Aguirre','Velasquez','calle 20B','1996-01-23',1),
(8,'Maria','Deicy','Arias','Lozano','calle 1B','1994-08-17',1),
(9,'Isabel','Cristina','Aristizabal','Salazar','calle 1B','1976-03-23',1),
(10,'Alexandra','','Ariza','Caceres','calle 1B','1992-09-24',1),
(11,'Claudia','Cristina','Ortiz','Paez','calle 112D','1979-08-17',1),
(12,'Gustavo','Rodolfo','Arenas','Vega','calle 13E','1964-03-23',2),
(13,'Rafael','Dario','Ortiz','','carrera 41B','1982-09-24',2);



INSERT INTO telefono_perso values
(1,'3185274636',1),
(2,'3004172636',2),
(3,'3161273636',3),
(4,'3162284636',5),
(5,'3154142231',7),
(6,'3161273636',8),
(7,'3203372124',10),
(8,'3204175636',4),
(9,'3181113629',6),
(10,'3116922832',9),
(11,'3294175636',12),
(12,'3111113629',13),
(13,'3216922832',11);


INSERT INTO cliente values
(1,'2014-08-7',1,1),
(2,'2014-08-7',1,2),
(3,'2014-08-7',1,7),
(4,'2014-08-7',1,3);

INSERT INTO salario values
(1,'activo',9000,'2012-10-18','noviembre'),
(2,'activo',10000,'2012-1-18','enero'),
(3,'activo',12000,'2012-3-18','marzo'),
(4,'activo',15000,'2012-3-18','marzo'),
(5,'activo',25000,'2012-3-18','marzo');


INSERT INTO area_trabajo values
(1,'cajero'),
(2,'administrador'),
(3,'finanzas'),
(4,'repositor Externo'),
(5,'personal'),
(6,'stick master');

INSERT INTO empleado values
(1,'2012-9-18',1,'Maria','hola123',8,3,2),
(2,'2014-6-28',1,'Isabel','ola123',9,1,1),
(3,'2013-10-8',1,'judith','hla123',4,1,1),
(4,'2012-9-18',1,'juan','hola1234',5,5,1),
(5,'2014-6-28',1,'Alexandra','seola123',10,4,3),
(6,'2013-10-8',1,'rocio','hiqla123',6,2,5);


INSERT INTO tipo_concepto values
(1,'concepto 1'),
(2,'concepto 2'),
(3,'concepto 3'),
(4,'concepto 4'),
(5,'concepto 5'),
(6,'concepto 6');


INSERT INTO concepto_salario values
(1,'salario 1',1200,3),
(2,'salario 2',900,1),
(3,'salario 3',1400,2),
(4,'salario 4',15000,5),
(5,'salario 5',1000,4),
(6,'salario 6',400,6);

INSERT INTO detalle_salario values
(1,10,300,3),
(2,40,700,1),
(3,65,100,2),
(4,10,200,6),
(5,40,900,4),
(6,65,500,5);


INSERT INTO unidad_presentacion values
(1,'unidad 1'),
(2,'unidad 2'),
(3,'unidad 3'),
(4,'unidad 4'),
(5,'unidad 5'),
(6,'unidad 6');

INSERT INTO categoria values
(1,'Bebidas'),
(2,'Ropa y accesorios'),
(3,'Alimentos'),
(4,'tecnologia'),
(5,'Los bienes de uso com�n');


INSERT INTO subcategoria values
(1,'Bebidas alcholicas',1),
(2,'frutas',3),
(3,'jeans',2),
(4,'Bebidas no alcholicas',1),
(5,'camisas',2),
(6,'Bebidas gaseosas',1),
(7,'Bebidas alcholicas',1),
(8,'lacteos',3),
(9,'electrodomesticos',4),
(10,'quimicos',5),
(11,'television',4);

INSERT INTO elemento values
(1,'cerveza',1),
(2,'manzana',2),
(3,'jeans blue',3),
(4,'camisa red',5),
(5,'lulo',2),
(6,'pan',2),
(7,'leche',8),
(8,'naranja',2),
(9,'guanabana',2),
(10,'computador',9),
(11,'televisor',11),
(12,'arroz',2),
(13,'azucar',1),
(14,'pasta',3),
(15,'horno microondas',9),
(16,'jabon',10);


INSERT INTO proveedor values
(1,'2014-1-30','A','Bavaria','4362837',11),
(2,'2012-8-20','A','Coca Cola','4378927',12),
(3,'2013-5-18','A','Nutresa','4387298',13),
(4,'2014-5-18','D','Corral','4378728',12);



INSERT INTO suministro values
(1,'2012-1-30',40000,'A','200',1),
(2,'2013-8-20',35000,'A','200',2),
(3,'2015-5-18',25000,'A','200',1),
(4,'2012-1-30',40000,'A','200',3),
(5,'2013-8-20',35000,'A','200',3),
(6,'2015-5-18',25000,'A','200',1),
(7,'2012-1-30',40000,'A','200',3),
(8,'2013-8-20',35000,'A','200',2),
(9,'2015-5-18',25000,'A','200',2);


INSERT INTO producto values
(1,'20',16,10,2,3,2),
(2,'30',16,40,1,3,1),
(3,'45',16,50,3,1,3);


INSERT INTO detalle_suministro values
(1,'20',1298,'1995-3-20',100,2,13),
(2,'30',1612,'2015-2-3',400,1,12),
(3,'45',11236,'2015-4-28',512,3,11),
(4,'50',18376,'2014-8-12',140,4,3),
(5,'320',2316,'2013-10-1',120,5,4),
(6,'415',34126,'2012-2-13',340,6,5),
(7,'28',92836,'2011-1-16',192,9,6),
(8,'39',91836,'2009-7-23',420,8,8),
(9,'15',76286,'2014-5-27',500,7,9);

INSERT INTO factura_venta values
(1,'2014-2-10',3504,2500,1,1),
(2,'2014-4-18',1762,3000,1,2),
(3,'2014-9-16',7892,4500,2,3),
(13,'2015-9-16',1823,1928349,1,2),
(14,'2015-2-05',2393,1232837,5,3),
(15,'2015-2-03',3293,1828349,6,4);

INSERT INTO detalle_venta values
(1,28304,2500,20192,2000,15,1,1),
(2,31504,1500,2192,2200,12,2,3),
(3,12504,3500,1192,20310,14,3,12),
(4,94204,1200,12192,22100,12,5,2),
(5,3904,250,24192,11130,12,4,3),
(6,21204,500,3412,34000,14,2,4),
(7,35104,400,2392,8700,16,1,2),
(8,65304,100,45192,82700,12,3,5),
(9,87204,3400,40192,19200,14,2,6),
(10,918204,4500,10192,28001,12,3,7),
(11,3504,200,56192,92800,15,4,2),
(12,3504,290,2122,783000,13,6,2);

INSERT INTO credito values
(1,'activo',3501214,'2012-2-10',250120,191120384,1),
(2,'activo',17631342,'2011-4-10',3120,17326849,2),
(3,'inactivo',2931262,'2013-3-10',781292,451202130,3),
(4,'activo',190312,'2014-4-10',351310,19113837,2),
(5,'inactivo',131262,'2015-10-10',712542,45281700,4),
(6,'activo',3502124,'2012-2-10',253100,19120384,5),
(7,'activo',176312,'2011-4-10',33100,1736849,6),
(8,'inactivo',2921262,'2013-3-10',781292,4502130,7),
(9,'activo',1931302,'2014-4-10',331400,1913837,8),
(10,'inactivo',1113262,'2015-10-10',751242,4581700,9);

INSERT INTO abono_credito values
(1,351204,'2014-2-10',1),
(2,331094,'2014-2-10',2),
(3,453104,'2014-2-10',4),
(4,52122304,'2014-2-10',5),
(5,3131504,'2014-2-10',6),
(6,312094,'2014-2-10',10),
(7,453104,'2014-2-10',9),
(8,521204,'2014-2-10',7);


para cambiar datos es asi 

update detalle_suministro set fecha_vensumin='2015-03-02'
where iddetallesuministro=1;



