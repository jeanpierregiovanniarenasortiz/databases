tarea de base de datos:

CREATE DATABASE ejercicio;

USE  ejercicio;

CREATE TABLE genero(idgenero INT(10) primary key not null,
nombre_gene VARCHAR(10));

CREATE TABLE persona (idpersona INT(10) primary key not null,
nombre1 VARCHAR(15),
nombre2 VARCHAR(15),
apellido1 VARCHAR(15),
apellido2 VARCHAR(15),
direccion VARCHAR(15),
fecha_naci DATE,
fidgenero INT(10),
constraint fk_perso_genero foreign key(fidgenero)
references genero(idgenero) on delete cascade
on update cascade) engine= InnoDB;

CREATE TABLE telefono_perso (idtelefono INT(10) primary key not null,
numtelefonico NUMERIC(15,0),
fidpersona INT(10),
constraint telefo_perso foreign key (fidpersona)
references persona (idpersona) on delete cascade
on update cascade)engine= InnoDB;

CREATE TABLE cliente (idcliente INT(10) primary key not null,
fecha_registro_cliente DATE,
estado_cliente BOOLEAN,
fidpersona INT(10),
constraint cliente_perso foreign key (fidpersona)
references persona (idpersona) on delete cascade
on update cascade)engine= InnoDB;

CREATE TABLE salario (idsalario INT(10) primary key not null,
estado_salario VARCHAR(10),
valor_total_salario NUMERIC(15,2),
fecha_pago DATE,
mes_pago VARCHAR(40));

CREATE TABLE area_trabajo (idareatrabajo INT(10) primary key not null,
nombre_area_traba VARCHAR(30));

CREATE TABLE empleado (idempleado INT(10) primary key not null,
fecha_ingreso DATE,
estado_empleado BOOLEAN,
nombre_b_usuario VARCHAR(40),
contrasena VARCHAR(20),
fidpersona INT(10),
fidsalario INT(10),
fidareatrabajo INT(10),
constraint empleado_perso foreign key (fidpersona)
references persona (idpersona),
constraint empleado_salario foreign key (fidsalario)
references salario (idsalario),
constraint empleado_area_trabajo foreign key (fidareatrabajo)
references area_trabajo (idareatrabajo));

CREATE TABLE tipo_concepto (idtipoconcepto INT(10) primary key not null,
nombre_tipo_concepto VARCHAR(100));


CREATE TABLE concepto_salario (idconceptosalario INT(10) primary key not null,
nombre_b_concepto VARCHAR(100),
valor_concepto DECIMAL(10,2),
fidtipoconcepto INT(10),
constraint concep_sala_tipo_concep foreign key (fidtipoconcepto)
references tipo_concepto (idtipoconcepto) on delete cascade
on update cascade);

CREATE TABLE detalle_salario (iddetallesalario INT(10) primary key not null,
canti_concepto NUMERIC(65),
valor_concepto_ds DECIMAL(10,2),
fidconceptosalario INT(10),
constraint detalle_sala_concep_sala foreign key (fidconceptosalario)
references concepto_salario (idconceptosalario) on delete cascade
on update cascade)engine= InnoDB;

CREATE TABLE unidad_presentacion (idunidadpresentacion INT(10) primary key not null,
unidad VARCHAR(30));

CREATE TABLE categoria (idcategoria INT(10) primary key not null,
nombre_categoria VARCHAR(20));

CREATE TABLE subcategoria (idsubcategoria INT(10) primary key not null,
nombre_subcategoria VARCHAR(20),
fidcategoria INT(10),
constraint fcategoria foreign key (fidcategoria)
references categoria (idcategoria) on delete cascade
on update cascade);

CREATE TABLE elemento (idelemento INT(10) primary key not null,
nombre_elemento VARCHAR(65),
fidsubcategoria INT(10),
constraint fsubcategoria foreign key (fidsubcategoria)
references subcategoria (idsubcategoria) on delete cascade
on update cascade);



CREATE TABLE proveedor (idproveedor INT(10) primary key not null,
fecha_registro DATE,
estado_prov ENUM('A','D'),
nombre_empresa VARCHAR(15),
telefono_empresa INT(30),
fidpersona INT(10),
constraint proveedor_perso foreign key (fidpersona)
references persona (idpersona));

CREATE TABLE suministro (idsuministro INT(10) primary key not null,
fecha_sumin DATE,
valor_total_sumin NUMERIC(50,2),
estado_sumin ENUM('A','D'),
valor_iva_sumin NUMERIC(5,2),
fidproveedor INT(10),
constraint suministro_proveedor foreign key (fidproveedor)
references proveedor (idproveedor));



CREATE TABLE producto (idproducto INT(10) primary key not null,
canti_present INT(65),
porcentaje_iva INT(65),
stock_actual INT(10),
fidelemento INT(10),
fidproveedor INT(10),
fidunidadpresentacion INT(10),
fidetalleventa INT(10),
constraint producto_elemento foreign key (fidelemento)
references elemento (idelemento),
constraint producto_proveedor foreign key (fidproveedor)
references proveedor (idproveedor),
constraint producto_uni_pre foreign key (fidunidadpresentacion)
references unidad_presentacion (idunidadpresentacion),
constraint producto_detalleven foreign key (fidetalleventa)
references detalle_venta (idetalleventa));

CREATE TABLE detalle_suministro (iddetallesuministro INT(10) primary key,
cantidad_suministrada INT (65),
valor_sumin_inis NUMERIC(65,2),
fecha_vensumin DATETIME,
valor_uni_sumin NUMERIC(65,2),
fidsuministro INT(10),
fidproducto INT(10),
constraint detallesuministro foreign key (fidsuministro)
references suministro (idsuministro),
constraint detalleproducto foreign key (fidproducto)
references producto (idproducto));



CREATE TABLE factura_venta (idfacturaventa INT(10) primary key not null,
fecha_venta DATE,
valor_iva_fact NUMERIC(20,2),
valor_total_venta NUMERIC(20,2),
valor_subtotal NUMERIC (20,2),
fidempleado INT(10),
fidcliente INT(10),
constraint fact_empleado foreign key (fidempleado)
references empleado (idempleado),
constraint fact_cliente foreign key (fidcliente)
references cliente (idcliente));

CREATE TABLE detalle_venta (iddetalleventa INT(10) primary key,
valor_total_salida NUMERIC(65,2),
cantidad_salida NUMERIC(65,0),
valor_unitario NUMERIC(65,2),
valor_ganancia NUMERIC(65,2),
porcentaje_ganancia INT(10),
fidfacturaventa INT(10),
fidproducto INT(10),
constraint detalle_fact foreign key (fidfacturaventa)
references factura_venta (idfacturaventa),
constraint detalle_producto foreign key (fidproducto)
references producto (idproducto));


CREATE TABLE credito (idcredito INT(10) primary key not null,
estado_credito VARCHAR(10),
valor_total DECIMAL(65,2),
fecha_credito DATE,
valor_credito NUMERIC(65,2),
saldo_actual NUMERIC(65,2),
fidfacturaventa INT(10),
constraint credito_fact foreign key (fidfacturaventa)
references factura_venta (idfacturaventa));

CREATE TABLE abono_credito (idabonocredito INT(10) primary key not null,
valor_abono DECIMAL(65,2),
fecha_abono DATE,
fidcredito INT(10),
constraint abono_credito foreign key (fidcredito)
references credito (idcredito));

